<?php

namespace Chainside\LaravelAPI;

use Illuminate\Support\Facades\Facade;

class ChainsideAPIFacade extends Facade {

    /**
     * Create Facade Accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'chainsideapi'; }

}