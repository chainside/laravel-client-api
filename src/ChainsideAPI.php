<?php

namespace Chainside\LaravelAPI;

use Chainside\LaravelAPI\Connection\ApiContext;
use Chainside\LaravelAPI\Exceptions\ChainsideAPIArgumentsException;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class ChainsideAPI
 * @package Chainside\LaravelAPI
 */
class ChainsideAPI
{

    /**
     * @var array
     */
    private $entities = [
        'invoices.create'    => 'Chainside\LaravelAPI\API\Invoices\Create\Request',
        'invoices.get'       => 'Chainside\LaravelAPI\API\Invoices\Get\Request',
        'invoices.get_all'   => 'Chainside\LaravelAPI\API\Invoices\GetAll\Request',
        'invoices.update'    => 'Chainside\LaravelAPI\API\Invoices\Update\Request',
        'invoices.delete'    => 'Chainside\LaravelAPI\API\Invoices\Delete\Request',
    ];

    /**
     * @var ApiContext
     */
    private $context;

    /**
     * ChainsideAPI constructor.
     *
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;
	}

    /**
     * @param $entity
     *
     * @return \Chainside\LaravelAPI\API\ChainsideAPIRequest
     */
    public function make($entity)
    {
        $restEntity = $this->getClassName($entity);

        return new $restEntity($this->context);
    }

    /**
     * @param $entity
     *
     * @return mixed
     * @throws ChainsideAPIArgumentsException
     */
    private function getClassName($entity)
    {
        self::validateEntity($entity);

        return $this->entities[$entity];
    }

    /**
     * @param $entity
     *
     * @throws ChainsideAPIArgumentsException
     */
    private function validateEntity($entity)
    {
        if (!in_array($entity, array_keys($this->entities)))
            throw new ChainsideAPIArgumentsException('Factory error, entity not instantiable', 0);
    }

}