<?php

namespace Chainside\LaravelAPI\Connection;

use League\Uri\Schemes\Http as HttpUri;
use League\Uri\Components\Query;

class Credentials
{
    private $uuid;
    private $apikey;

    public function __construct($uuid, $apikey)
    {
        $this->uuid = $uuid;
        $this->apikey = $apikey;
    }


    public function getUUID()
    {
        return $this->uuid;
    }


    public function buildSignature($payload)
    {
        return base64_encode(hash_hmac('sha1', $payload, $this->apikey, true));
    }
}