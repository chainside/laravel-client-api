<?php

namespace Chainside\LaravelAPI\Connection;

class ApiContext
{
    private $api_version;
    private $base_url;
    private $timeout;

    private $credentials;


    public function __construct(array $config)
    {
        $this->api_version = $config['api_version'];
        $this->timeout = $config['timeout'];
        $this->base_url = $config[$config['mode']]['base_url'];

        $this->credentials = new Credentials(
            $config[$config['mode']]['credentials']['uuid'],
            $config[$config['mode']]['credentials']['apikey']
        );
    }

    public function getBaseUrl()
    {
        return $this->base_url . '/' . $this->api_version . '/';
    }

    public function getTimeout()
    {
        return $this->timeout;
    }

    public function getCredentials()
    {
        return $this->credentials;
    }

}