<?php

namespace Chainside\LaravelAPI\API;

use Chainside\LaravelAPI\Connection\ApiContext;
use Chainside\LaravelAPI\Exceptions\ChainsideAPIArgumentsException;
use Chainside\LaravelAPI\Exceptions\ChainsideAPIConnectionException;
use Chainside\LaravelAPI\Parameters\ParamsValidator;
use Faker;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\RequestInterface;


abstract class ChainsideAPIRequest
{
    /**
     * @var ApiContext
     */
    protected $context;

    /**
     * @var Client
     */
    protected $client;
    /**
     * @var array
     */
    protected $options = [];
    /**
     * @var Faker\Generator
     */
    private $faker;

    /**
     * ChainsideAPIRequest constructor.
     *
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;
        $this->faker = Faker\Factory::create();
        $this->client = $this->buildClient();
    }

    /**
     * @return Client
     */
    private function buildClient()
    {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());
        $stack->push($this->add_authentication());

        return new Client([
            'timeout'  => $this->context->getTimeout(),
            'base_uri' => $this->context->getBaseUrl(),
            'handler'  => $stack,
        ]);
    }

    private function add_authentication()
    {
        return function (callable $handler) {
            return function (RequestInterface $request, array $options) use ($handler) {
                $request = $request->withHeader('X-UUID', $this->context->getCredentials()->getUUID());
                $request = $request->withHeader('X-Signature', $this->context->getCredentials()->buildSignature($request->getBody()));
//                echo "adding authentication\n";
//                echo "request body type: " . get_class($request->getBody()) ."\n";
//                echo "request body: " . $request->getBody() ."\n";
//                echo "request headers: " . json_encode($request->getHeaders()) ."\n";
//                echo "request target: " . $request->getRequestTarget() ."\n";
//                echo "request uri: " . $request->getUri() ."\n";
//                echo "request method: " . $request->getUri() ."\n";
                return $handler($request, $options);
            };
        };
    }

    // between query, json, multipart, form_params

    /**
     * @return mixed
     * @throws ChainsideAPIArgumentsException
     * @throws ChainsideAPIConnectionException
     */
    public function send()
    {
        $validator = new ParamsValidator($this->options, $this->getParametersRules());

        if ($validator->fails())
            throw new ChainsideAPIArgumentsException('Something wrong with parameters', 0);

        return $this->doRequest();
    }

    protected abstract function getParametersRules();

    /**
     * @param $method
     * @param $url
     *
*@return mixed
     * @throws ChainsideAPIConnectionException
     */
    private function doRequest()
    {
        $start_time = round(microtime(true) * 1000);

        try {
            $response = $this->client->request($this->getMethod(), $this->getEndpoint(), $this->buildOptions());

        } catch (\Exception $e) {
            throw new ChainsideAPIConnectionException($e->getMessage(), $e->getCode());
        }

        $end_time = round(microtime(true) * 1000);
        $execution_time = $end_time - $start_time;

        return $this->toResponse($response, $this, $execution_time);
    }

    protected abstract function getMethod();

    protected abstract function getEndpoint();

    public function buildOptions()
    {
        return [
            'headers'               => [
                'User-Agent' => $this->faker->useragent,
                'Accept'     => "application/json",
            ],
            $this->getPayloadType() => $this->options,
        ];
    }

    protected abstract function getPayloadType();

    /**
     * @param $raw_response
     * @param $request
     * @param $execution_time
     *
     * @return mixed
     */
    protected abstract function toResponse(Response $raw_response, ChainsideApiRequest $request, $execution_time);

    /**
     *
     * @param ChainsideAPIParameter $parameter
     */
    protected function buildOption(ChainsideAPIParameter $parameter)
    {
        $parameter->validate();
        $name = $parameter->getParameterName();
        $this->options[$name] = $parameter->data;
    }

}
