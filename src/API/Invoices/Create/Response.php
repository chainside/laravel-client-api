<?php

namespace Chainside\LaravelAPI\API\Invoices\Create;

use Chainside\LaravelAPI\API\ChainsideAPIResponse;


class Response extends ChainsideAPIResponse
{
    protected $invoice_id;
    protected $payment_url;
    protected $payment_address;
    protected $payment_amount;
    protected $exchange_rate;

    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * @return mixed
     */
    public function getPaymentUrl()
    {
        return $this->payment_url;
    }

    /**
     * @return mixed
     */
    public function getExchangeRate()
    {
        return $this->exchange_rate;
    }

    /**
     * @return mixed
     */
    public function getPaymentAddress()
    {
        return $this->payment_address;
    }

    /**
     * @return mixed
     */
    public function getPaymentAmount()
    {
        return $this->payment_amount;
    }

    protected function buildResponse()
    {
        $this->invoice_id = $this->raw_data->invoice_id;
        $this->payment_url = $this->raw_data->payment_url;
        $this->payment_address = $this->raw_data->payment_address;
        $this->payment_amount = $this->raw_data->payment_amount;
        $this->exchange_rate = $this->raw_data->exchange_rate;
    }

}
