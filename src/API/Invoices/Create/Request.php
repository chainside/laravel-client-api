<?php

namespace Chainside\LaravelAPI\API\Invoices\Create;

use Chainside\LaravelAPI\API\ChainsideAPIRequest;
use Chainside\LaravelAPI\Parameters\Amount;
use Chainside\LaravelAPI\Parameters\Description;
use Chainside\LaravelAPI\Parameters\FromCurrency;
use Chainside\LaravelAPI\Parameters\InvoiceType;
use Chainside\LaravelAPI\Parameters\ToCurrency;
use Chainside\LaravelAPI\Parameters\UrlCallback;
use Chainside\LaravelAPI\Parameters\UrlCancel;
use Chainside\LaravelAPI\Parameters\UrlConfirm;
use GuzzleHttp\Psr7\Response as Psr7Response;

/**
 * Class Request
 * @package Chainside\LaravelAPI\API\Invoices
 */
class Request extends ChainsideAPIRequest
{
    /**
     * @var string
     */
    public static $endpoint = "invoice";

    public static $method = "POST";

    public static $payload_type = "json";

    protected static $parameters_rules = [
        "type"          => "required",
        "from_currency" => "required",
        "to_currency"   => "required",
        "amount"        => "required",
        "description"   => "required",
        "url_confirm"   => "required",
        "url_cancel"    => "required",
        "url_callback"  => "required",
    ];

    public function setInvoiceType($type)
    {
        $this->buildOption(new InvoiceType($type));
    }

    public function setFromCurrency($currency)
    {
        $this->buildOption(new FromCurrency($currency));
    }

    public function setToCurrency($currency)
    {
        $this->buildOption(new ToCurrency($currency));
    }

    public function setAmount($amount)
    {
        $this->buildOption(new Amount($amount));
    }

    public function setDescription($description)
    {
        $this->buildOption(new Description($description));
    }

    public function setUrlConfirm($url_confirm)
    {
        $this->buildOption(new UrlConfirm($url_confirm));
    }

    public function setUrlCancel($url_cancel)
    {
        $this->buildOption(new UrlCancel($url_cancel));
    }

    public function setUrlCallback($url_callback)
    {
        $this->buildOption(new UrlCallback($url_callback));
    }

    /**
     * @return string
     */
    protected function getEndpoint()
    {
        return self::$endpoint;
    }

    /**
     * @return string
     */
    protected function getMethod()
    {
        return self::$method;
    }

    protected function getParametersRules()
    {
        return self::$parameters_rules;
    }

    protected function getPayloadType()
    {
        return self::$payload_type;
    }

    /**
     * @param $raw_response
     * @param $request
     * @param $execution_time
     *
     * @return Response
     */
    protected function toResponse(Psr7Response $raw_response, ChainsideAPIRequest $request, $execution_time)
    {
        return new Response($raw_response, $request, $execution_time);
    }
}