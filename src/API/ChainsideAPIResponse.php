<?php

namespace Chainside\LaravelAPI\API;

use Chainside\LaravelAPI\Exceptions\ChainsideAPIMalformedRequestException;
use Chainside\LaravelAPI\Exceptions\ChainsideAPIResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

abstract class ChainsideAPIResponse
{
    public $request_uuid;
    public      $execution_time;
    protected   $raw_data;
    private $raw_response;

    public function __construct(Response $raw_response, ChainsideAPIRequest $request, $execution_time)
    {
        $this->request_uuid = rand();
        $this->execution_time = $execution_time;
        $this->raw_response = $raw_response;

        if ($raw_response->getStatusCode() == "422")
            throw new ChainsideAPIMalformedRequestException('Malformed request', 0);

        if($raw_response->getStatusCode() != "200")
            throw new ChainsideAPIResponseException($raw_response->getStatusCode(), $raw_response->getReasonPhrase(), $raw_response->getBody());

        $this->raw_data = json_decode($raw_response->getBody()->getContents());

        $this->buildResponse();
    }

    protected abstract function buildResponse();

    public function getExecutionTime()
    {
        return $this->execution_time;
    }

    public function getRawResponse()
    {
        return $this->raw_response;
    }

}
