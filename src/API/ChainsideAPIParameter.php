<?php

namespace Chainside\LaravelAPI\API;

use Chainside\LaravelAPI\Exceptions\ChainsideAPIArgumentsException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

abstract class ChainsideAPIParameter
{
    protected static $choices      = [];
    protected static $default      = null;
    protected static $parameter_name;
    public $data;

    public function __construct($data)
    {
        $this->data = method_exists($this, 'setData') ? $this->setData($data) : $data;
    }

    public function getParameterName()
    {
        return static::$parameter_name;
    }

    public function validate()
    {
        return $this->validateChoice();
    }

    private function validateChoice($value = null)
    {
        $value = is_null($value) ? $this->data : $value;
        if (sizeof(static::$choices) > 0 && !in_array($value, static::$choices)) {
            echo "wrong choice $value for param " . static::$parameter_name . "\n";
            throw new ChainsideAPIArgumentsException('Wrong parameter, valid values: ' . json_encode(static::$choices), 0);
        }

        return true;
    }

    public function __toString()
    {
        return $this->data;
    }
}
