<?php

namespace Chainside\LaravelAPI\Parameters;


use Chainside\LaravelAPI\API\ChainsideAPIParameter;

class Amount extends ChainsideAPIParameter
{
    protected static $parameter_name = "amount";

}