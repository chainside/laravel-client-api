<?php

namespace Chainside\LaravelAPI\Parameters;


use Chainside\LaravelAPI\API\ChainsideAPIParameter;

class UrlCancel extends ChainsideAPIParameter
{
    protected static $parameter_name = "url_cancel";
}