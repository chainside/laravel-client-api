<?php

namespace Chainside\LaravelAPI\Parameters;


use Chainside\LaravelAPI\API\ChainsideAPIParameter;

class UrlConfirm extends ChainsideAPIParameter
{
    protected static $parameter_name = "url_confirm";
}