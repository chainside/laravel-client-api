<?php
/**
 * Created by PhpStorm.
 * User: lorenzo
 * Date: 14/05/16
 * Time: 17.34
 */

namespace Chainside\LaravelAPI\Parameters;


class ParamsValidator
{
    protected $options;
    protected $rules;

    public function __construct(array $options, array $rules)
    {
        $this->options = $options;
        $this->rules = self::parseRules($rules);
    }

    private static function parseRules(array $rules)
    {
        $parsed_rules = [];

        foreach ($rules as $parameter => $param_rules) {
            $parsed_rules[$parameter] = explode("|", $param_rules);
        }

        return $parsed_rules;
    }

    public function fails()
    {
        return !$this->passes();
    }

    public function passes()
    {
        return $this->validateImplicit() && $this->validateExplicit();
    }

    private function validateImplicit()
    {
        foreach ($this->rules as $param_name => $param_rules) {
            if (in_array("required", $param_rules)) {
                if (!isset($this->options[$param_name])) {
                    return false;
                }
            }
        }

        return true;
    }

    private function validateExplicit()
    {
        return true;
    }

}