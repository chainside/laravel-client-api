<?php

namespace Chainside\LaravelAPI\Parameters;


use Chainside\LaravelAPI\API\ChainsideAPIParameter;

class UrlCallback extends ChainsideAPIParameter
{
    protected static $parameter_name = "url_callback";
}