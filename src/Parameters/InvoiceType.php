<?php

namespace Chainside\LaravelAPI\Parameters;

use Chainside\LaravelAPI\API\ChainsideAPIParameter;


class InvoiceType extends ChainsideAPIParameter
{
    protected static $parameter_name = "type";
    protected static $choices = ['classic', 'express', 'preauth'];
}