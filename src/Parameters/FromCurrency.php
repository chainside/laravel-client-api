<?php

namespace Chainside\LaravelAPI\Parameters;


use Chainside\LaravelAPI\API\ChainsideAPIParameter;
use Chainside\LaravelAPI\Exceptions\ChainsideAPIArgumentsException;

class FromCurrency extends ChainsideAPIParameter
{
    protected static $parameter_name = "from_currency";

    public function setData($data)
    {
        if(strlen($data) != 3)
            throw new ChainsideAPIArgumentsException(self::$parameter_name . " malformed: " . $data, 0);

        return $data;
    }
}