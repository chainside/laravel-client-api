<?php

namespace Chainside\LaravelAPI\Parameters;


use Chainside\LaravelAPI\API\ChainsideAPIParameter;

class Description extends ChainsideAPIParameter
{
    protected static $parameter_name = "description";
}