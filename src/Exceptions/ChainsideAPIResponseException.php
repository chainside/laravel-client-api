<?php

namespace Chainside\LaravelAPI\Exceptions;

class ChainsideAPIResponseException extends \Exception
{

    public $httpStatusCode;
    public $error;
    public $content;

    public function __construct($httpStatusCode, $error, $content)
    {
        parent::__construct('Error contacting chainside, response code: ' . $httpStatusCode .'. Error: ' . $error, 0);
        $this->httpStatusCode= $httpStatusCode;
        $this->error = $error;
        $this->content = $content;
    }

}