<?php

namespace Chainside\LaravelAPI\Exceptions;


class ChainsideAPIArgumentsException extends \Exception
{

    public $code;
    public $message;

    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }

}