<?php

namespace Chainside\LaravelAPI\Exceptions;


class ChainsideAPIConnectionException extends \Exception
{

    public $code;
    public $message;

    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }

}