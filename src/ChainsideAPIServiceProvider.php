<?php

namespace Chainside\LaravelAPI;

use Illuminate\Support\ServiceProvider;
use Chainside\LaravelAPI\Connection\ApiContext;

class ChainsideAPIServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $configPath = __DIR__ . '/../config/chainside.php';

        $this->publishes([$configPath => config_path('chainside.php')]);

        $this->mergeConfigFrom($configPath, 'chainside');
    }

    /**
     * Register the mandrill service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('chainsideapi', function($app){
            $context = new ApiContext($app['config']['chainside']);
            return new ChainsideAPI($context);
        });
    }

}


