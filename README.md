# INSTALLATION

Add package to composer.json

"require": {
    "chainside/laravel-api": "dev-master"
},
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/chainside/laravel-client-api"
    }
],

Add the servicer provider into config/app.php

```
        Chainside\LaravelAPI\ChainsideAPIServiceProvider::class,
```
Create the alias for the Facade

```
        'ChainsideAPI' => Chainside\LaravelAPI\ChainsideAPIFacade::class,
```
Publish the configuration file into config directory:

php artisan vendor:publish --provider="Chainside\LaravelAPI\ChainsideAPIServiceProvider"
