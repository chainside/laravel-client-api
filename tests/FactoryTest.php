<?php

namespace Chainside\LaravelAPI;

use Chainside\LaravelAPI\Connection\ApiContext;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testFactorySuccess()
    {
        $chainside = $this->getInstance();
        $invoices_create = $chainside->make('invoices.create');
        $this->assertInstanceOf('\Chainside\LaravelAPI\API\Invoices\Create\Request', $invoices_create);
        // TODO: add other requests type
    }

    public function testFactoryFails()
    {
        $chainside = $this->getInstance();
        $this->setExpectedException('\Chainside\LaravelAPI\Exceptions\ChainsideAPIArgumentsException');
        $chainside->make('nothing');
    }

    private function getConfig()
    {
        return include __DIR__ . '/../config/chainside.php';
    }

    private function getInstance()
    {
        $config = $this->getConfig();
        $context = new ApiContext($config);
        return new ChainsideAPI($context);
    }
}
