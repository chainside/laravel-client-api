<?php

namespace Chainside\LaravelAPI;

use Chainside\LaravelAPI\Parameters\ParamsValidator;

class ParamsValidatorTest extends \PHPUnit_Framework_TestCase
{

    public function testRequiredSuccessValidation()
    {
        $validator = new ParamsValidator([
            'param1' => 'value',
            'param2' => 'value',
        ], [
            'param1' => 'required',
            'param2' => 'required',
        ]);
        $this->assertTrue($validator->passes());

        $validator = new ParamsValidator([
            'param1' => 'value',
            'param3' => '',
        ], [
            'param1' => '',
            'param2' => '',
        ]);
        $this->assertTrue($validator->passes());
    }

    public function testRequiredFailsValidation()
    {
        $validator = new ParamsValidator([
            'param1' => 'value',
        ], [
            'param1' => 'required',
            'param2' => 'required',
        ]);
        $this->assertFalse($validator->passes());
    }
}