<?php

namespace Chainside\LaravelAPI;

use Chainside\LaravelAPI\Connection\ApiContext;


class InvoicesTest extends \PHPUnit_Framework_TestCase
{

//    public function testInvoicesFail()
//    {
//        $chainside = $this->getInstance();
//        $invoices_create = $chainside->make('invoices.create');
//
//        $this->setExpectedException('\Chainside\LaravelAPI\Exceptions\ChainsideAPIArgumentsException');
//
//        $invoices_create->setInvoiceType('something');
//        $invoices_create->send();
//    }

    public function testInvoicesSuccess()
    {
        $chainside = $this->getInstance();
        $invoices = $chainside->make('invoices.create');
        $invoices->setInvoiceType('classic');
        $invoices->setFromCurrency('eur');
        $invoices->setToCurrency('btc');
        $invoices->setAmount('123.56');
        $invoices->setDescription('classic invoice');
        $invoices->setUrlConfirm('http://www.test.it/url');
        $invoices->setUrlCancel('http://www.test.it/url');
        $invoices->setUrlCallback('http://www.test.it/url');

        $res = $invoices->send();

        echo $res->getInvoiceId() . "\n";
        echo $res->getApprovalUrl() . "\n";
        echo $res->getPaymentAddress() . "\n";

        $this->assertInstanceOf('\Chainside\LaravelAPI\API\Invoices\Create\Response', $res);
    }
//
//
//    public function testInvoicesSuccess2()
//    {
//        $expected_url = "https://maps.googleapis.com/maps/api/directions/json?output_format=json&origin=41.904783333333%2C12.483083333333&destination=41.9057166%2C12.4820393&mode=driving&alternatives=true&avoid=highways&units=metric&region=IT";
//        $from = new Point(41.904783333333, 12.483083333333);
//        $to = new Point(41.9057166, 12.4820393);
//        $w1 = new Waypoint(new Point(42.9057166, 13.4820393));
//        $w2 = new Waypoint(new Point(43.9057166, 14.4820393));
//
//
//        $gmaps = $this->getInstance();
//        $invoices = $gmaps->make('directions');
//        $invoices->setOrigin($from);
//        $invoices->setWaypoints([$w1, $w2]);
//        $invoices->setDestination($to);
//        $invoices->setMode('driving');
//        $invoices->setAlternatives('true');
//        $invoices->setAvoid('highways');
//        $invoices->setUnit('metric');
//        $invoices->setRegion('IT');
//        $res = $invoices->send();
//        var_dump($res->request_url);
//        var_dump($res->execution_time);
//
//        $this->assertInstanceOf('LorenzoGiust\LaravelGoogleMapsAPI\API\Invoices\Response', $res);
//    }

    private function getInstance()
    {
        $config = $this->getConfig();
        $context = new ApiContext($config);

        return new ChainsideAPI($context);
    }

    private function getConfig()
    {
        return include __DIR__ . '/../config/chainside.php';
    }

}