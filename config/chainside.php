<?php

return [

    "mode"        => "sandbox",
    "api_version" => "v1",

    "live" => [
        "base_url"    => "https://api.chainside.net",
        "credentials" => [
            "uuid"   => "",
            "apikey" => "",
        ],
    ],

    "sandbox" => [
        "base_url"    => "https://sandbox.api.chainside.net",
        "credentials" => [
            "uuid"   => "",
            "apikey" => "",
        ],
    ],

    "timeout" => 20,

];